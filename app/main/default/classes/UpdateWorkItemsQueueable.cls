public with sharing class UpdateWorkItemsQueueable implements Queueable,Database.AllowsCallouts {
    private Map<String, Set<id>> deploymentTypes;
    private Map<String,Set<Integer>> workItemsByOrg;
    private List<Tfsrequest> tfsRequests;
    
    public void execute(QueueableContext context) {
        if(this.tfsRequests!=null && !this.tfsRequests.isEmpty()){
            Tfsrequest req=tfsRequests.get(0);
            fetchWI(req.orgName,req.wid,req.deptype);
            
            this.tfsRequests.remove(0);
            if(!this.tfsRequests.isEmpty()){
                System.enqueueJob(new UpdateWorkItemsQueueable(this.tfsRequests));
            }
        }
    }
    public  UpdateWorkItemsQueueable(List<Tfsrequest> tfsRequests){
        this.tfsRequests=tfsRequests;
    }
    
    public static void fetchWI(String orgsStr, Integer wid, String deptype){
        String endpoin= '/'+orgsStr+'/_apis/wit/workitems?ids='+ wid+'&$expand=Fields&api-version=5.0';    
        System.debug(endpoin);
        HttpResponse res= sf_tfs.TFSRequests.fetchWorkItems( 'GET',  endpoin);
        
        
        if(res!=null && res.getStatusCode()==200){
            
            sf_tfs.TFS_FeatchWorkItems   workItems=sf_tfs.TFS_FeatchWorkItems.parse(res.getBody());
            List<sf_tfs.TFS_FeatchWorkItems.Value> wiList=workItems.value;
            if(wiList!=null && !wiList.isEmpty()){
                sf_tfs.TFS_FeatchWorkItems.Value workItem=wiList.get(0);
                system.debug(workItem.fields.System_TeamProject);
                system.debug(workItem.fields.System_State); 
                system.debug(workItem.fields.System_WorkItemType);

                
             List<sf_tfs__Work_Item__c>  workItemTypeList=new List<sf_tfs__Work_Item__c>([SELECT 
                                                                                          id, sf_tfs__Default_State__c,
                                                                                          Name, 
                                                                                          sf_tfs__Project__r.Name 
                                                                                          FROM sf_tfs__Work_Item__c 
                                                                                          WHERE Name=:workItem.fields.System_WorkItemType
                                                                                          AND sf_tfs__Project__r.Name=:workItem.fields.System_TeamProject]);
                system.debug(workItemTypeList);
                if(!workItemTypeList.isEmpty()){
                    
                  sf_tfs__Work_Item__c workItemType =workItemTypeList.get(0);
                    system.debug(workItemType);
                    if( workItemType.sf_tfs__Default_State__c!=null && !String.isEmpty(workItemType.sf_tfs__Default_State__c)){
                        system.debug(workItemType.sf_tfs__Default_State__c);
                        Map<String,String> status= (Map<String,String>)System.JSON.deserialize(workItemType.sf_tfs__Default_State__c, Map<String,String>.class);
                        String statuss;
                        if(deptype=='Developer/Production'){
                            system.debug(deptype);
                            if(status.containsKey('toProduction')){
                                 system.debug('toProduction');
                                statuss=status.get('toProduction');
                            }
                        }else if(deptype=='Sandbox'){
                             system.debug(deptype);
                            if(status.containsKey('toSandbox')){
                                statuss=status.get('toSandbox');
                                 system.debug('toSandbox');
                            }
                        }
                        system.debug(statuss);
                        if(statuss!=null){
                            String body = '[{"op": "add","path": "/fields/System.State","value": "'+statuss+'"}]';
                            String endpoint = '/'+EncodingUtil.urlEncode(orgsStr, 'UTF-8')+'/'+EncodingUtil.urlEncode(workItem.fields.System_TeamProject, 'UTF-8')+'/_apis/wit/workitems/'+wid+'?api-version=5.0';
                            
                            
                            endpoint=endpoint.replace('+', '%20');
                            

                            system.debug(endpoint);
                            Http http = new Http();
                            HttpRequest req = new HttpRequest();
                            
                            req.setHeader('X-HTTP-Method-Override','PATCH');
                            req.setEndPoint('callout:'+TFSSetup.getNamedCreds()+endpoint);
                            req.setHeader('Content-Type', 'application/json-patch+json');
                            req.setBody(body);
                            
                            req.setMethod('POST');
                            try{
                                system.debug(req.getEndPoint());
                                HTTPResponse resp = http.send(req); 
                                      system.debug(resp.getBody());
                             system.debug(resp.getStatus());
                             system.debug(resp.getStatusCode());
                            }catch(CalloutException e){
                                system.debug(e.getMessage());
                            }
                            
                      
                        }

                    }
                }
                
            }
            
            
        }else{
            
        }   
        
        
        
        
    }
    
    
    public UpdateWorkItemsQueueable( Map<String, Set<id>> deploymentTypes){
        system.debug(deploymentTypes);
        Set<id> deploymentIds=new Set<id>();
        this.deploymentTypes=deploymentTypes;
         Map<id,String> dtypes= new Map<id,String>();
        for(String dType:this.deploymentTypes.keySet()){
            system.debug('');
            deploymentIds.addAll(this.deploymentTypes.get(dType));
            for(id did:this.deploymentTypes.get(dType)){
                dtypes.put(did,dType);
            }
  
        }
        
       
        
        
        
        
        
        List<sf_tfs__Flosum_To_TFS__c> refList=new List<sf_tfs__Flosum_To_TFS__c>([SELECT Id, sf_tfs__key__c, sf_tfs__Keys__c, sf_tfs__Organization__c ,Name
                                                                                   FROM sf_tfs__Flosum_To_TFS__c 
                                                                                   WHERE Name IN: deploymentIds]);
        Map<id,sf_tfs__Flosum_To_TFS__c> refByParent=new Map<id,sf_tfs__Flosum_To_TFS__c>();
        for(sf_tfs__Flosum_To_TFS__c ref:refList){
            refByParent.put(ref.Name,ref);
        }
        workItemsByOrg= new Map<String,Set<Integer>>();
        
        for(sf_tfs__Flosum_To_TFS__c ref:refByParent.values()){
            
            if(dtypes.containsKey(ref.Name)){
                String depType=dtypes.get(ref.Name);
                if(depType!=null){
                    if(ref.sf_tfs__Organization__c!=null && !String.isEmpty(ref.sf_tfs__Organization__c)){
                        Map<String, Set<Integer>> keys=(Map<String, Set<Integer>>)System.JSON.deserialize(ref.sf_tfs__Organization__c, Map<String, Set<Integer>>.class);
                        
                        
                        for(String orgName:keys.keySet()){
                            system.debug(depType);
                            if(workItemsByOrg.containsKey(orgName+'-'+depType)){
                                Set<Integer> wids=workItemsByOrg.get(orgName+'-'+depType);
                                wids.addAll(keys.get(orgName));
                                workItemsByOrg.put(orgName+'-'+depType,wids);
                            }else{
                                workItemsByOrg.put(orgName+'-'+depType,keys.get(orgName));
                            }
                        }
                    }   
                }
   
            }
            

        }
        this.tfsRequests=new List<Tfsrequest>();
        system.debug(workItemsByOrg);
        for(String org: workItemsByOrg.keySet()){
            for(Integer wid:workItemsByOrg.get(org)){
                this.tfsRequests.add(new Tfsrequest(org,wid));
            }
        }
    }
    public class Tfsrequest{
        public String deptype;
        public String orgName;
        public Integer wid;
        public Tfsrequest(String orgName,Integer wid){
            String[] orgAndType=orgName.split('-');
            
            this.wid=wid;
            this.orgName=orgAndType[0];  
            this.deptype=orgAndType[1];  
        }
    }
    
    public static void test(){
        Integer i = 0 ;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
    }
}